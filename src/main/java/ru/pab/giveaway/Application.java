package ru.pab.giveaway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.pab.giveaway.config.JpaConfig;
import ru.pab.giveaway.config.SpringMvcConfig;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(new Class<?>[] {
				Application.class,
				JpaConfig.class,
				SpringMvcConfig.class
		}, args);
	}
}
