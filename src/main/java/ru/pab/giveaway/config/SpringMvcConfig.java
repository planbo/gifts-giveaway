package ru.pab.giveaway.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @author Andrey Platunov
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "ru.pab.giveaway")
public class SpringMvcConfig implements WebMvcConfigurer {

    @Value("${spring.mvc.view.prefix}")
    private String viewPrefix;
    @Value("${spring.mvc.view.suffix}")
    private String viewSuffix;

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.jsp(viewPrefix, viewSuffix);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/js/");
    }
}
