package ru.pab.giveaway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pab.giveaway.entity.Gift;
import ru.pab.giveaway.service.ActionGiftService;
import ru.pab.giveaway.utils.ActionGiftUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Platunov
 */

@RestController
public class ActionGiftController {

    @Autowired
    private ActionGiftService actionGiftService;

    @RequestMapping(value = "/take-action-and-available-gifts/{actionId}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> takeActionAndAvailableGifts(@PathVariable(value = "actionId")
                                                                                   String actionId) {
        Map<String, Object> result;
        Integer actionIdInt = ActionGiftUtils.tryToParseStringToInteger(actionId);
        if (actionIdInt == null) {
            result = new HashMap<>();
            result.put(ActionGiftUtils.KEY_ACTION_NOT_FOUND, true);
        } else {
            result = actionGiftService.getActionAndAvailableGifts(actionIdInt);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/take-one-gift/{actionId}/{giftId}", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> takeOneGift(@PathVariable(value = "actionId") String actionId,
                                                           @PathVariable(value = "giftId") String giftId) {
        Map<String, Object> result;
        Integer actionIdInt = ActionGiftUtils.tryToParseStringToInteger(actionId);
        Integer giftIdInt = ActionGiftUtils.tryToParseStringToInteger(giftId);

        if (actionIdInt != null && giftIdInt != null) {
            result = actionGiftService.removeGiftPiece(actionIdInt, giftIdInt);
        } else {
            result = new HashMap<>();

            if (actionId == null) {
                result.put(ActionGiftUtils.KEY_ACTION_NOT_FOUND, true);
            }

            if (giftIdInt == null) {
                result.put(ActionGiftUtils.KEY_GIFT_NOT_FOUND, true);
            }
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/find-gifts-by-word/{word}", method = RequestMethod.GET)
    public ResponseEntity<List<Gift>> findGiftsByWord(@PathVariable(value = "word", required = false) String word) {
        List<Gift> result = actionGiftService.getAvailableGiftsByWord(word);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
