package ru.pab.giveaway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.pab.giveaway.service.ActionGiftService;
import ru.pab.giveaway.utils.ActionGiftUtils;

import java.util.Map;

/**
 * @author Andrey Platunov
 */

@Controller
public class MainController {

    @Autowired
    private ActionGiftService actionGiftService;

    @RequestMapping(value = "/action/{actionId}", method = RequestMethod.GET)
    public ModelAndView getPage(@PathVariable String actionId, Model model) {
        Integer actionIdInt = ActionGiftUtils.tryToParseStringToInteger(actionId);
        if (actionIdInt != null) {
            Map<String, Object> result = actionGiftService.getActionAndAvailableGifts(actionIdInt);
            model.addAllAttributes(result);
        }
        return new ModelAndView("action");
    }
}
