package ru.pab.giveaway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pab.giveaway.entity.Action;

/**
 * @author Andrey Platunov
 */
public interface ActionRepository extends JpaRepository<Action, Integer> {

    @Query("select a from Action a where a.id = :id")
    Action getActionById(@Param("id") Integer id);
}
