package ru.pab.giveaway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.pab.giveaway.entity.Gift;

import java.util.List;

/**
 * @author Andrey Platunov
 */
public interface GiftRepository extends JpaRepository<Gift, Integer> {

    @Query("select a from Gift a where a.id = :id")
    Gift getGiftById(@Param("id") Integer id);

    @Query("select a from Gift a where a.amount > 0 order by name")
    List<Gift> getAllAvailableGifts();

    @Query("select a from Gift a where a.amount > 0 and a.name like %:name% order by name")
    List<Gift> getAvailableGiftsByName(@Param("name") String name);
}
