package ru.pab.giveaway.service;

import ru.pab.giveaway.entity.Action;
import ru.pab.giveaway.entity.Gift;

import java.util.List;
import java.util.Map;

/**
 * @author Andrey Platunov
 */
public interface ActionGiftService {

    Action getActionById(Integer actionId);

    Gift getGiftById(Integer giftId);

    List<Gift> getAllAvailableGifts();

    Map<String, Object> getActionAndAvailableGifts(Integer actionId);

    Map<String, Object> removeGiftPiece(Integer actionId, Integer giftId);

    List<Gift> getAvailableGiftsByWord(String word);
}
