package ru.pab.giveaway.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pab.giveaway.entity.Action;
import ru.pab.giveaway.entity.Gift;
import ru.pab.giveaway.repository.ActionRepository;
import ru.pab.giveaway.repository.GiftRepository;
import ru.pab.giveaway.utils.ActionGiftUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Platunov
 */

@Service
public class ActionGiftServiceImpl implements ActionGiftService {

    @Autowired
    ActionRepository actionRepository;
    @Autowired
    GiftRepository giftRepository;

    @Override
    @Transactional
    public Action getActionById(Integer actionId) {
        return actionRepository.getActionById(actionId);
    }

    @Override
    @Transactional
    public Gift getGiftById(Integer giftId) {
        return giftRepository.getGiftById(giftId);
    }

    @Override
    @Transactional
    public List<Gift> getAllAvailableGifts() {
        return giftRepository.getAllAvailableGifts();
    }

    @Override
    @Transactional
    public Map<String, Object> getActionAndAvailableGifts(Integer actionId) {
        Map<String, Object> result = new HashMap<>();
        Action action = getActionById(actionId);
        List<Gift> gifts = getAllAvailableGifts();
        result.put(ActionGiftUtils.KEY_ACTION, action);
        result.put(ActionGiftUtils.KEY_GIFTS, gifts);
        if (action != null) {
            result.put(ActionGiftUtils.KEY_ACTION_DEPLETED, action.getAmount() <= 0);
            return result;
        } else {
            result.put(ActionGiftUtils.KEY_ACTION_NOT_FOUND, true);
        }
        return result;
    }

    @Override
    @Transactional
    public Map<String, Object> removeGiftPiece(Integer actionId, Integer giftId) {
        Map<String, Object> result = getActionAndAvailableGifts(actionId);

        Action action = getActionById(actionId);
        if (action == null) {
            return result;
        }
        int actionAmount = action.getAmount();

        Gift gift = getGiftById(giftId);
        if (gift == null) {
            result.put(ActionGiftUtils.KEY_GIFT_NOT_FOUND, true);
            return result;
        }
        int giftAmount = gift.getAmount();

        if (actionAmount > 0 && giftAmount > 0) {
            action.setAmount(--actionAmount);
            actionRepository.saveAndFlush(action);
            gift.setAmount(--giftAmount);
            giftRepository.saveAndFlush(gift);
            result = getActionAndAvailableGifts(actionId);
            result.put(ActionGiftUtils.KEY_GIFT_DEPLETED, false);
        } else {
            result.put(ActionGiftUtils.KEY_ACTION_DEPLETED, actionAmount <= 0);
            result.put(ActionGiftUtils.KEY_GIFT_DEPLETED, giftAmount <= 0);
        }

        return result;
    }

    @Override
    public List<Gift> getAvailableGiftsByWord(String word) {
        return giftRepository.getAvailableGiftsByName(word);
    }
}
