package ru.pab.giveaway.utils;

/**
 * @author Andrey Platunov
 */
public class ActionGiftUtils {

    public static final String KEY_ACTION           = "action";
    public static final String KEY_GIFTS            = "gifts";
    public static final String KEY_ACTION_DEPLETED  = "action_depleted";
    public static final String KEY_GIFT_DEPLETED    = "gift_depleted";
    public static final String KEY_ACTION_NOT_FOUND = "action_not_found";
    public static final String KEY_GIFT_NOT_FOUND   = "gift_not_found";

    public static Integer tryToParseStringToInteger(String stringToParse) {
        Integer result = null;
        try {
            result = Integer.parseInt(stringToParse);
        } catch (NumberFormatException e) {
            // do nothing, return null
        }
        return result;
    }
}
