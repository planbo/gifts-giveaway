var giftsGiveawayModule = angular.module("giftsGiveawayApp", []);

giftsGiveawayModule.controller("giftsGiveawayCtrl", function ($scope, $http, $location) {

    $scope.actionId = "";
    $scope.action = null;
    $scope.gifts = [];
    $scope.wildcard = "";

    var absUrl = $location.absUrl();
    if (absUrl.lastIndexOf("/") > -1) {
        var pieces = absUrl.split("/");
        $scope.actionId = pieces[pieces.length - 1];
    }

    $scope.takeActionAndAvailableGifts = function(){

        $http({
            method: "GET",
            url: "/take-action-and-available-gifts/" + $scope.actionId,
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            data: {}
        }).then(
            function(response) {
                $scope.action_not_found = response.data.action_not_found || false;
                $scope.action_depleted = response.data.action_depleted || false;
                $scope.action = response.data.action;
                $scope.gifts = response.data.gifts;
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.takeOneGift = function(giftId) {

        $http({
            method: "POST",
            url: "/take-one-gift/" + $scope.actionId + "/" + giftId,
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            data: {}
        }).then(
            function(response) {
                $scope.action_not_found = response.data.action_not_found || false;
                if ($scope.action_not_found) {
                    alert ("Акция не найдена.");
                }
                $scope.gift_not_found = response.data.gift_not_found || false;
                if ($scope.gift_not_found) {
                    alert ("Товар не найден.");
                }
                $scope.action_depleted = response.data.action_depleted || false;
                if ($scope.action_depleted) {
                    alert ("Количество товара по данной акции закончилось.");
                }
                $scope.gift_depleted = response.data.gift_depleted || false;
                if ($scope.gift_depleted) {
                    alert ("Товар закончился на складе.");
                }
                $scope.action = response.data.action;
                $scope.gifts = response.data.gifts;
            },
            function(errResponse) {
                alert("Ошибка: " + errResponse.toString());
            }
        );
    };

    $scope.findGiftsByWord = function(word) {

        if (!word || word == "") {
            alert("Введите строку для поиска.");
            return;
        }

        $http({
            method: "GET",
            url: "/find-gifts-by-word/" + word,
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            data: {}
        }).then(
            function(response) {
                $scope.gifts = response.data;
            },
            function(errResponse) {
                alert("Ошибка: " + errResponse.toString());
            }
        );
    };

    $scope.takeActionAndAvailableGifts();
});