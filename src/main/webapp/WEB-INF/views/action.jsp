<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html ng-app="giftsGiveawayApp">
    <head>
        <script defer src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
        <script defer src="/js/app.js"></script>
        <meta charset="utf-8">
        <title>Акция</title>
    </head>
    <body>
        <div ng-controller="giftsGiveawayCtrl"/>
        <div ng-switch on="action_not_found">
            <div ng-switch-when=true>
                <header>
                    <h2>Акция c id {{actionId}} не найдена</h2>
                </header>
            </div>
            <div ng-switch-default>
                <header>
                    <h2>Акция "{{action.name}}"!</h2>
                </header>
                <div ng-switch on="action_depleted">
                    <div ng-switch-when=true>
                        <p>К сожалению, по данной акции подарков не осталось.</p>
                    </div>
                    <div ng-switch-default>
                        <p>Осталось подарков: {{action.amount}}</p>
                        <p>
                            <input size="40" ng-model="wildcard"/>
                            <button type="button" ng-click="findGiftsByWord(wildcard)">Фильтровать</button>
                            <button type="button" ng-click="takeActionAndAvailableGifts()">Показать все</button>
                        </p>
                        <p>
                            <div ng-repeat="gift in gifts track by $index">
                                <text type="text" size ="50">{{gift.name}}</text>
                                <button type="button" ng-click="takeOneGift(gift.id)" ng-model="gift">Забрать</button>
                            </div>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>